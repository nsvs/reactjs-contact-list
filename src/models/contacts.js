import { message } from 'antd';
import {
	queryContacts,
	queryContact,
	queryContactsByName,
	queryContactsBySurname,
	queryContactsByTag,
	submitContact,
	updateContact,
	deleteContact,
	connectContactToTag
} from '../services/api';

export default {
    namespace: 'contacts',
  
    state: {
      list: [],
    },
  
    effects: {
      *fetchContacts(_, { put, call, select }) {
        const response = yield call(queryContacts);
        if (response.length) {
          yield put({
            type: 'loadContactsToState',
            payload: response,
          });
        } else {
          message.error("Error getting data");
        }
      },
      *fetchContact({payload}, { call, put }) {
        const response = yield call(queryContact, payload);
        yield put({
          type: 'loadContactToState',
          payload: response,
        });
      },
      *saveContact({ payload }, { put, call, select }) {
				const response = yield call(submitContact, payload);
				// response.code?
				// nemam ID sa servera.. 
				// ako se stigne, pronaci nacin za zakacit DB ID
        
				message.success("Contact successfully saved!");
				yield put({
					type: 'saveContactToState',
					payload: payload,
				});

      },
      *editContact({ payload }, { put, call }) {
        const response = yield call(updateContact, payload);
				message.success("Contact successfully updated!");
				yield put({
					type: 'updateContactToState',
					payload: payload,
				});
      },
      *removeContact({ payload }, { call, put }) {
        const response = yield call(deleteContact, payload);
				message.success("Contact deleted");
				yield put({
					type: 'removeContactFromState',
					payload: payload,
				});
      },
    },
  
    reducers: {
			loadContactsToState(state, { payload }) {
				const withKey = [...payload];
				withKey.forEach(element => {
          element.key = element.id;
          element.isTemp = false;
				});

				return {
					...state,
					list: withKey,
				};
      },
      // fetch single
      loadContactToState(state, { payload }) {
        const withFetched = [...state.list, payload];
				return {
					...state,
					list: withFetched,
				};
			},
      saveContactToState(state, { payload }) {
        // generiraj random key i id
				payload.key = Math.floor(Math.random() * (2000 - 150 + 1)) + 150;
        payload.id = payload.key;
        // kako su random key i id, necu dozvolit brisanje ni edit
        payload.isTemp = true;
        return {
          ...state,
          list: [...state.list, payload],
        };
      },
      updateContactToState(state, { payload }) {        
        // payload.key = payload.id;
        const withoutUpdated = state.list.filter(contact => {
          return contact.id !== payload.id;
        });
        const indexOfUpdated = state.list.findIndex(contact => contact.id === payload.id);
        // zbog zadrzavanja mjesta gdje je bio redak u index tablici
        withoutUpdated.splice(indexOfUpdated, 0, payload);
        return {
          ...state,
          list: withoutUpdated,
        };
      },
      removeContactFromState(state, action) {
        const withoutDeleted = state.list.filter(contact => {
          return contact.id !== action.payload;
        });
        return {
          ...state,
          list: withoutDeleted,
        };
      },
    },
  };
  
