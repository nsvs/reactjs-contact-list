import React, { PureComponent, Fragment } from 'react';
import { message, Modal, Form,  Input, Icon } from 'antd';

// props inputName, headerName, createVisible, connectValueToParent

export default class CreateModal extends PureComponent {

  state = {
    value: '', 
  }

  handleOk = (e) => {
    const { value } = this.state;
    const { inputName, connectValueToParent } = this.props;
    
    if (!value) {
      return message.error('Please fill out required fields');
    }
    if (inputName === "emails") {
      // validate email anystring@anystring.anystring
      const re = /\S+@\S+\.\S+/;
      if (!re.test(value)) {
        return message.error('Invalid email address.');
      }
    }
        
    // uplift value state

    connectValueToParent(inputName, value);
    this.setState({value: ''});

  }

  handleCancel = (e) => {
    const { handleModalHide } = this.props;
    handleModalHide();
  }

  handleInputChange = (e) => {
    const { value } = e.target;
    this.setState({ value: value });
  }

  render () {
    const { inputName, createVisible, headerName } = this.props;
    const { value } = this.state;
    const modalTitle = `Create new ${headerName}`;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (
      <Fragment>
        <Modal
          title={modalTitle}
          visible={createVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form.Item
            {...formItemLayout}
            label={headerName}
            required
          >
            <Input
              name={inputName}
              placeholder={modalTitle}
              prefix={<Icon type="input" style={{ color: 'rgba(0,0,0,.25)' }} />}
              value={value}
              onChange={this.handleInputChange}
            />
          </Form.Item>
        </Modal>
      </Fragment>
    )
  };
}
