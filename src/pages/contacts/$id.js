import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Row, Col, Input, Icon, List, Button } from 'antd';
import CreateModal from '../../components/createModal';


class ContactView extends PureComponent {
  state = { 
    name: '',
    surname: '',
    address: '',
    phoneNumbers: [],
    contactTags: [],
    emails: [],
    createNumberModalVisible: false,
    createEmailModalVisible: false,
    createTagModalVisible: false,
  };

  componentDidMount() {
    const { list, match } = this.props;
    const { dispatch } = window.g_app._store;
    // format za fetch kad je u stateu lista
    this.formatList();

    // pogodena je ruta direktno bez entrya na index page, dohvati kontakt i spremi ga u state
    if (!list.length) {
      dispatch({
        type: 'contacts/fetchContact',
        payload: match.params.id
      });
    }
  }

  componentWillReceiveProps() {
    // format za fetch na direktnom upadu na rutu && update iz reduxa
    this.formatList();
  }

  formatList = () => {
    const { match, list } = this.props;
    const activeContactArray = list.filter(contact => contact.id === parseInt(match.params.id));
    const activeContact = {...activeContactArray[0]};
    this.setState({
      name: activeContact.name,
      surname: activeContact.surname,
      address: activeContact.address,
      phoneNumbers: typeof activeContact.phoneNumbers !== 'undefined' ? activeContact.phoneNumbers.map(value => value.number) : [],
      contactTags: typeof activeContact.phoneNumbers !== 'undefined' ? activeContact.contactTags.map(value => value.tag.name) : [],
      emails: typeof activeContact.emails !== 'undefined' ? activeContact.emails.map(value => value.emailAddress) : []
    });
  }

  handleContactInputChange = (e) => {
    const { value } = e.target;
    const stateName = e.target.name;
    this.setState({ [stateName]: value });
  }

  handleModalHide = () => {
    this.setState({createNumberModalVisible: false, createEmailModalVisible: false, createTagModalVisible: false });
  }

  // connectToChildComponent
  handleModalSubmit = (submitType, value) => {
    const statePropValues = [ ...this.state[submitType] ];
    statePropValues.push(value);
    // TODO: provuc visibleStateIme modala kroz propse pa vratit kao parametar u funkciju
    // ili ovo ...
    this.setState( { [submitType]: statePropValues,  createNumberModalVisible: false, createEmailModalVisible: false, createTagModalVisible: false } );
  }

  showNumbersModal = () => {
    this.setState({createNumberModalVisible: true });
  }

  showEmailsModal = () => {
    this.setState({createEmailModalVisible: true });
  }

  showTagsModal = () => {
    this.setState({createTagModalVisible: true });
  }

  handleDispatchToApi = () => {
    const { dispatch } = window.g_app._store;
    const { match } = this.props;
    const { name, surname, address, phoneNumbers, contactTags, emails } = this.state;
    const payload = {
      id: parseInt(match.params.id),
      name,
      surname,
      address,
      emails: emails.map((value) => {
        return {
          emailAddress: value
        }
      }),
      phoneNumbers: phoneNumbers.map((value) => {
        return {
          number: value
        }
      }),
      contactTags: contactTags.map((value) => {
        return {
          tag: {
            name: value
          }
        }
      })
    };
    dispatch({
      type: 'contacts/editContact',
      payload
    });
  }

  handleListItemRemove = (itemType, value) => {
    const statePropValues = [ ...this.state[itemType] ];
    const filtered = statePropValues.filter(item => item !== value);
    this.setState( { [itemType]: filtered} );
  }


  render () {
    const { name, surname, address, phoneNumbers, contactTags, emails, createNumberModalVisible, createEmailModalVisible, createTagModalVisible } = this.state;
    const { loading } = this.props;
  
    return (
      <Fragment>
        <CreateModal 
          createVisible={createTagModalVisible} 
          // state input name za ID propa u stateu
          inputName="contactTags" 
          connectValueToParent={this.handleModalSubmit}
          handleModalHide={this.handleModalHide}
          headerName="tag"
        />
        <CreateModal 
          createVisible={createNumberModalVisible} 
          inputName="phoneNumbers" 
          connectValueToParent={this.handleModalSubmit}
          handleModalHide={this.handleModalHide}
          headerName="number"
        />
        <CreateModal 
          createVisible={createEmailModalVisible} 
          inputName="emails" 
          connectValueToParent={this.handleModalSubmit}
          handleModalHide={this.handleModalHide}
          headerName="email"
        />
        <Row>
          <div style={{ marginBottom: 8, float: 'left'}}>
            <Button icon="save" type="default" onClick={this.handleDispatchToApi}>
              Save changes
            </Button>
          </div>
        </Row>
        <div style={{ background: '#ECECEC', padding: '30px' }}>
        <Row gutter={16}>
          <Col span={8}>
            <Card title="name" bordered={false} loading={loading}>
              <Input
                  name="name"
                  placeholder="Enter contact name"
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  value={name}
                  onChange={this.handleContactInputChange}
                />
            </Card>
          </Col>
          <Col span={8}>
            <Card title="surname" bordered={false} loading={loading}>
              <Input
                  name="surname"
                  placeholder="Enter contact surname"
                  prefix={<Icon type="team" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  value={surname}
                  onChange={this.handleContactInputChange}
              />
            </Card>
          </Col>
          <Col span={8}>
            <Card title="address" bordered={false} loading={loading}>
              <Input
                  name="address"
                  placeholder="Enter contact address"
                  prefix={<Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  value={address}
                  onChange={this.handleContactInputChange}
              />
            </Card>
          </Col>
        </Row>
      </div>
        <div style={{ background: '#ECECEC', padding: '30px' }}>
        <Row gutter={16}>
          <Col span={8}>
            <Card title="phoneNumbers" extra={<Button type="dashed" shape="circle" icon="plus" onClick={this.showNumbersModal} />}  bordered={false} loading={loading}>
              <List
                bordered
                dataSource={phoneNumbers}
                renderItem={item => (<List.Item actions={[<Button type="dashed" size="small" onClick={() => this.handleListItemRemove("phoneNumbers", item)} style={{float: 'right'}} shape="circle" icon="delete" /> ]}>{item} </List.Item>)}
              />
            </Card>
          </Col>
          <Col span={8}>
            <Card title="emails" extra={<Button type="dashed" shape="circle" icon="plus" onClick={this.showEmailsModal} />} bordered={false} loading={loading}>
              <List
                  bordered
                  dataSource={emails}
                  renderItem={item => (<List.Item actions={[<Button type="dashed" size="small" onClick={() => this.handleListItemRemove("emails", item)} style={{float: 'right'}} shape="circle" icon="delete" /> ]}>{item}</List.Item>)}
              />
            </Card>
          </Col>
          <Col span={8}>
            <Card title="tags" extra={<Button type="dashed" shape="circle" icon="plus" onClick={this.showTagsModal} />} bordered={false} loading={loading}>
              <List
                  bordered
                  dataSource={contactTags}
                  renderItem={item => (<List.Item actions={[<Button type="dashed" size="small" onClick={() => this.handleListItemRemove("contactTags", item)} style={{float: 'right'}} shape="circle" icon="delete" /> ]}>{item}</List.Item>)}
              />
            </Card>
          </Col>
        </Row>

      </div>
    </Fragment>
    )
  };
}

function mapStateToProps(state) {
  return {
    list: state.contacts.list,
    loading: state.loading.models.contacts
  };
}

export default connect(mapStateToProps)(ContactView);
