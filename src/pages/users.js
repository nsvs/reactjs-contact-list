import router from 'umi/router';
import { Button } from 'antd';
import styles from './users.css';

export default function() {
  return (
    <div className={styles.normal}>
      <h1>Page users</h1>
      <Button type="danger" onClick={() => { router.goBack(); }}> Click to go back</Button>
    </div>
  );
}
