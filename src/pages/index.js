import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Popconfirm, Form, Icon, Button, Modal, Row, Col, Table, Divider, Input, message } from 'antd';
import Link from 'umi/link';

class Contacts extends PureComponent {
  state = { 
    modalVisible: false,
    contactName: '',
    contactSurname: '',
    contactAddress: '',
    searchText: '',
  };
  
  componentDidMount() {
    const { dispatch } = window.g_app._store;
    dispatch({
      type: 'contacts/fetchContacts',
    });
  }


  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  }

  handleOk = (e) => {
    const { contactAddress, contactName, contactSurname} = this.state;
    const { dispatch } = window.g_app._store;
    
    // osnovna validacija
    if (!contactAddress || !contactName || !contactSurname) {
      // ne zatvaraj modal
      return message.error('Please fill out required fields');
    }

    // dispatch na server
    dispatch({
      type: 'contacts/saveContact',
      payload: {
        address: contactAddress,
        name: contactName,
        surname: contactSurname,
        isFavourite: false
      }
    });
    // pocisti formu i zatvori modal
    this.setState({
      modalVisible: false,
      contactAddress: '',
      contactName: '',
      contactSurname: ''
    }, () => this.handlePageReload() );
  }

  handleCancel = (e) => {
    this.setState({
      modalVisible: false,
    });
  }

  // isprazni contactName focusaj na njega
  emitEmptyContactName = () => {
    this.contactNameInput.focus();
    this.setState({ contactName: '' });
  }

  emitEmptyContactSurname = () => {
    this.contactSurnameInput.focus();
    this.setState({ contactSurname: '' });
  }
  
  emitEmptyContactAddress = () => {
    this.contactAddressInput.focus();
    this.setState({ contactAddress: '' });
  }

  onNewContactInputChange = (e) => {
    const { value } = e.target;
    const stateName = e.target.name;
    this.setState({ [stateName]: value });
  }

  handleContactRemove = (id) => {
    const { dispatch } = window.g_app._store;
    dispatch({
      type: 'contacts/removeContact',
      payload: id
    });
  }

  handlePageReload = () => {
    // dohvatit ce ponovno kontakte, tako da imaju pravi id iz baze
    const { dispatch } = window.g_app._store;
    dispatch({
      type: 'contacts/fetchContacts',
    });
  }

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys, selectedKeys, confirm, clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => { this.searchInput = node; }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => text,
  })

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  render () {
    // table columns
    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '25%',
      ...this.getColumnSearchProps('name'),
    }, {
      title: 'Surname',
      dataIndex: 'surname',
      key: 'surname',
      width: '25%',
      ...this.getColumnSearchProps('surname'),
    }, {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
      width: '25%',
    },  {
      title: 'Action',
      key: 'action',
      width: '25%',
      render: (text, record) => {
        if (record.isTemp) {
          return (
            <Popconfirm
              title="This contact cannot be deleted at this moment. Reload page?"
              onConfirm={() => this.handlePageReload()}
            >
              <a>TempDelete</a>
            </Popconfirm>
          );
        }
        const { id } = record;
        const route = `/contacts/${id}`;
        return (
          <span>
            <Link to={route}>View details</Link>
            <Divider type="vertical" />
            <Popconfirm
              title="Are you sure you want to remove this contact?"
              onConfirm={() => this.handleContactRemove(record.id)}
            >
              <a>Delete</a>
            </Popconfirm>
          </span>
        );
      }
    }];
    const { list, loading } = this.props;
    const { contactName, contactSurname, contactAddress } = this.state;
    const suffixName = contactName ? <Icon type="close-circle" onClick={this.emitEmptyContactName} /> : null;
    const suffixSurname = contactSurname ? <Icon type="close-circle" onClick={this.emitEmptyContactSurname} /> : null;
    const suffixAddress = contactAddress ? <Icon type="close-circle" onClick={this.emitEmptyContactAddress} /> : null;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    return (
      <Row>
        <Row>
          <div style={{ marginBottom: 16, float: 'left'}}>
            <Button type="primary" onClick={this.showModal}>
              Create new contact
            </Button>
          </div>
        </Row>
        <Modal
          title="Create new contact"
          visible={this.state.modalVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form.Item
            {...formItemLayout}
            label="Name"
            required
          >
            <Input
              // name => koristit ce se za set i clear
              name="contactName"
              placeholder="Enter contact name"
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              suffix={suffixName}
              value={contactName}
              onChange={this.onNewContactInputChange}
              ref={node => this.contactNameInput = node}

            />
          </Form.Item>
          <Form.Item
            {...formItemLayout}
            label="Surname"
            required
          >
            <Input
              name="contactSurname"
              placeholder="Enter contact surname"
              prefix={<Icon type="team" style={{ color: 'rgba(0,0,0,.25)' }} />}
              suffix={suffixSurname}
              value={contactSurname}
              onChange={this.onNewContactInputChange}
              ref={node => this.contactSurnameInput = node}

            />
          </Form.Item>
          <Form.Item
            {...formItemLayout}
            label="Address"
            required
          >
            <Input
              name="contactAddress"
              placeholder="Enter contact address"
              prefix={<Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />}
              suffix={suffixAddress}
              value={contactAddress}
              onChange={this.onNewContactInputChange}
              ref={node => this.contactAddressInput = node}

            />
          </Form.Item>                  
        </Modal>
        <Table loading={loading} dataSource={list} columns={columns} />
      </Row>
      
    );
  }
}

// redux model-state na this.props
function mapStateToProps(state) {
  const { list } = state.contacts;
  return {
    list,
    // za loading animaciju
    loading: state.loading.models.contacts
  };
}

export default connect(mapStateToProps)(Contacts);