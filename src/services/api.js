import request from '../utils/request';

// API routes

export async function queryContacts() {
  return request('/api/contacts/');
}

export async function queryContact(id) {
  return request(`/api/contacts/${id}`);
}

export async function queryContactsByName(name) {
  return request(`/api/contacts/name/${name}`);
}

export async function queryContactsBySurname(surname) {
    return request(`/api/contacts/surname/${surname}`);
}

export async function queryContactsByTag(id) {
    return request(`/api/contacts/tag/${id}`);
  }

export async function submitContact(params) {
  return request('/api/contacts/create', {
    method: 'POST',
    body: params,
  });
}

export async function updateContact(params) {
  const { id } = params;
  return request(`/api/contacts/update/${id}`, {
    method: 'PUT',
    body: params,
  });
}

export async function deleteContact(id) {
  return request(`/api/contacts/${id}`, {
    method: 'DELETE',
  });
}

export async function connectContactToTag(params) {
  const { contactId, tagId } = params;
  return request(`/api/contacts/${contactId}/${tagId}`, {
    method: 'POST',
    body: params,
  });
}





