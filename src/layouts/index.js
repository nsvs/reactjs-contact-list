import { Button, Layout, Menu, Breadcrumb } from 'antd';
import router from 'umi/router';
const { Header, Content, Footer } = Layout;

function BasicLayout(props) {
  return (
    <Layout className="layout">
    <Header>
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['1']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1">Contacts ASP.NET/ReactJS app</Menu.Item>
      </Menu>
    </Header>
    <Content style={{ padding: '0 50px' }}>
      <div style={{ margin: '16px 0' }}>
          <Button type="primary"  icon="left" onClick={() => { router.goBack(); }}>Go back</Button>
      </div>
      <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>{ props.children }</div>
    </Content>
    <Footer style={{ textAlign: 'center' }}>
      Ant Design
    </Footer>
  </Layout>
  );
}

export default BasicLayout;
